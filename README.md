![logo](https://d1q6f0aelx0por.cloudfront.net/product-logos/a8957077-bbcd-41cc-b6d0-2924a1492e99-node.png)
# Node.js Training

Welcome to the Node.js Training. Here you'll find exercises that hopefuly will be good preparation for Node.js programming in "real life". **Important:** this is not a tutorial. There are tons of tutorials and documentations on the interent, so you'll have to find it yourself in order to complete every execrise. Don't worry - every exercise is focused on one topic. 

Before exploring the internet yourself, I'll Recommend you to be familiar with the [Node.js Official API](https://nodejs.org/docs/latest/api/). It is very well orginized and contains a lot of good and focused examples of just about everything. But of course Google is also a great resource of help.

## Order of exercises.

The exercises are divided into topics. The exercises are numberd according to the `TOPIC-EXERCISE` format, for example: `3-2` Meaning the 2nd exercise from the 3rd topic. 

Every topic aimes to stay individual, however - it is still important to cover them in their original order.

## Where do I Write My code?

For every student, 2 branches need to be crated: `NAME-dev` and `NAME-merged` (`NAME` will be replced by the student name. In your `NAME-dev` branch, create a dir for every topic, and then another dir for every exercise. 

For example, the path for the solution of `3-2` exercise is `3/2/index.js` in `NAME-DEV`.

Also, please use Visual Studio Code. This is by far the best editor for writing Node.js apllications.

## How Do I Submit My Code?

Via merge requests, of course!

After each exercise, a new merge request need to be opened, called after the exersice name. That means, marge request for **every** exercies.

For example, after Bob completes the `2-12` exercise, he will open a merge request called `2-12` (to be merged from `bob-dev` to `bob-merged`). If the merge request is accepted, there is now a `2/12` dir in the `NAME-merged` branch, meaning the exercies is aprroved by the reviewer.

**Please, follow all the instructions above.** Otherwise every student will submit its code in a different way and it will be impossible for reviewers to keep track.

# Topics:

## 1 - The very (very) basics
- ### 1-1 Hello $NAME$
    Write a program that gets a name as a command line argument and prints a nice greeting. For example:
    ```bash 
    node 1/1/index.js Bob
    # prints `Hello Bob`
    ```
    > Friednly reminder: your code should be placed in `1/1/index.js` in the `NAME-dev` branch. The main file of the exercise will always be named `index.js`.
    ----------
- ### 1-2 Number Sorting
    Write a program that gets a unkown number of numbers as a command line arguments and them print in order. For example:
    ```bash 
    node 1/2/index.js 5 8 -2 10 3 2
    # prints `-2 2 3 5 8 10` (in one line!)
    ```
    And:
    ```bash
    node 1/2/index.js 2 5 Bob 9
    # prints `Inavlid arg. "Bob" is not a number'
    ```
    ----------
- ### 1-3 Anagrams
    Write a program that gets a word and prints all its anagrams:
    ```bash 
    node 1/3/index.js abc
    # prints `
    # abc
    # acb
    # bca
    # bac
    # cab
    # cba
    # `
    ```
- ### 1-4 Countdown
    Write a program that counts down from X seconds to zero:
    ```bash 
    node 1/4/index.js 6
    # prints `
    # 6
    # 5
    # 4
    # 3
    # 2
    # 1
    # 0
    # `
    ```
    **Note:** the program should exit after X seconds.
## 2 - JS Modules
> Remember, The Node.js offical documentation is now your best friend: [Modules In Node](https://nodejs.org/docs/latest/api/modules.html)
- ### 2-1 Your First Module Ever
    In `2/1/index.js` write:
    ```js
    const myModule = require('./my-module');
    console.log(myModule.sort(2,5,6,1));
    ```
    Write a JS module that contains a `sort` function (thats sorts numbers) in `2/1/my-module.js` so:
    ```bash
    node 2/1/index.js
    # prints `[1, 2, 5, 6]` (in one line!)
    ```
    ----------
- ### 2-2 Randomizer
    Write a module that provides the following API:
    - `fromRange(min: number, max:number)` returns a random number between min and max (min<=RESULT<=max).
    - `fromArray(arr: any[])` returns a random element from array.
    - `fromString(str: string)` returns a random word from string

    So, for example:
    ```js
    // 2/2/index.js
    const randomizer = require('./randomizer');
    console.log(randomizer.fromRange(2,5)); // prints 3
    console.log(randomizer.fromArray([6,2,11])); // prints 11
    console.log(randomizer.fromString('Welcome to Japan!')); // prints 'Japan!'
    ```
## 3 - The `fs` Module.
> [fs documentaion](https://nodejs.org/docs/latest/api/fs.html) from the offical docs.
- ### 3-1 Famous Last Words
    Write a program that prints the last N word in a file. for example, if the file `myFile.txt` containts:
    ```
    Hello everybody! I just wanted to tell you that I love you all, this is it.
    ```
    So:
    ```bash
    node 3/1/index.js myFile.txt 3
    # prints `this is it.` (in one line!)
    node 3/1/index.js myFile.txt 5
    # prints `you all, this is it.` (in one line!)
    ```

    **Note:** don't use the synchronous versions of the methods (e.g `readFileSync`, instead use `readFile`). Make sure you understand why.

    ----------
- ### 3-2 Family Tree
    Write a module that gets and object and create a directory tree that looks like the object. For example:
    So:
    ```js
    // 3/2/index.js
    const createDirTree = require('./create-dir-tree');
    const tree = {
        
        'vegetables': {
            'tomato': {},
            'cucumber': {},
        },
        'fruits': {
            'lemons': {},
            'apples': {
                'red': {} 
                'green': {} 
            },
        },
    };
    createDirTree(tree);
    /*
    This will create a direcotry tree looks like this:
    vegetables
      tomato
      cucumber
    fruits
      lemons
      apples
        red
        green
    */
    ```
    ----------
- ### 3-3 LogSplitter
    Say you have an huge logfile (with 8K lines), and you want to split it to seperate files. 
    ```bash
    node 3/3/index.js huge.txt 80
    # will split `huge.txt` to 80 seperate files. (huge-1.txt, huge-2.txt [...] huge-80.txt)
    # So if 'hugefile.txt` got 8000 lines
    # every output file will have only 100 
    ```
    **Note:** you can assume that the file containts only text, and you should split the files by lines (so every output file will contain the same amount of lines).

    ----------

## 4 - Promises
> Promises are not even a Node thing, they are built in JS, so you won't find anything about them in the Node documentaion. However, google will help you. Also, i recommend [this tutorial](https://javascript.info/promise-basics).
- ### 4-1 readFile.then
    Promises are a (good) alternative to callbacks. since `fs.readFile` is expecting a callback, write a module that provides the same function but as e promise. For example:
    ```js
    // 4/1/index.js
    const fsPromises = require('./fs-promises'); //import the module you have written
    fsPromises.readFile('my-file.txt')
        .then((content) => {
            console.log(content); // prints the content of the file
        })
        .catch((err) => {
            console.error(err); // prints the error
        });
    ```
    ----------
- ### 4-2 Chains
    You can chain promises in order to make them happen one after another. Write a promise chain that:
    1. prints `hello`
    2. waits 3 seconds
    3. prints `world`
    2. waits 2 seconds
    3. writes `hello world` to `out.txt`
    3. prints `done`
    
    

## 5 - The `net` Module (TCP networking)
> [net documentaion](https://nodejs.org/docs/latest/api/net.html) from the offical docs.
- ### 5-1 Echo In TCP
    Write a tcp server that echoes whatever you send to it, then write a very simple tcp client to check if it works. (run the `5/1/server.js` and `5/1/client.js` concurrently).

    Read this after you complete the exercise:

    **Wait a minute..**
    Your sever should serve more than 1 client at a time (actually, this will happen by default). Node is single-threaded (meaning, everything is running on a single thread). so how our server is serving multiple clients? In order to achive asynchronicity, Node is using the `event loop` instead of multithreading. This is useful, and you don't need to figure out how to achive asynchronicity, but it is still important to remember that node **is** single-threaded, and it is important to understand how the event loop works. [This YouTube Video](https://www.youtube.com/watch?v=8aGhZQkoFbQ&t=822s) is doing a very good job at explaining the event loop. (feel free to watch it in 1.5X speed if your'e getting bored).

    ----------
- ### 5-2 Chatroom
    Tweak the server from the previous exercise so it will echo all the messages from the **other** clients, so you'll end up with some kind of a chatroom. For example, if you run 2 clients concurrently:
     ```bash
    node 5/2/client.js
    > Hello, I am Bob
    ```
    And in another client:
     ```bash
    node 5/2/client.js
    # prints: `someone saying: Hello, I am Bob`
    ```

    **Note:** for the first time in this training, you'll need to get a keyboard input from the user.

    ----------
- ### 5-3 Buffers In TCP
    Until now, we hanled a string messages. Now we will handle [Buffers](https://nodejs.org/docs/latest/api/buffer.html).

    Write A server That Provides the following Buffer API (all numbers are `Big Endian`):
    | Method  |  Opcode | Description |
    |---|---|---|
    | Sum | 0xA0  | Gets two `SInt16` numbers and return the sum of them (in `SInt16`)  |
    | Multiply  | 0xA1  | Gets two `SInt16` numbers and return the sum of them (in `SInt16`)    |
    | Average  | 0xA2  |  Gets The length of the list in `UInt8` and then N (N=length) instances of `SInt16` Values and returns the average value (in `float`) |

    so for example, if I want to calculate the sum of `2` and `3`, i'll have to send: 
    >0xA0 0x00 0x02 0x00 0x03

    If I want to calculate the average of `6 8 9`, I'll Have to send:
    >0xA2 0x03 0x00 0x06 0x00 0x08 0x00 0x09
    
    (0x03 is the length of the array).

    After you have written the server, write a module that acts as a client, and exports the `sum`,`multiply` and `average` methods.

## 6 - Third Party Modules (NPM)
`npm` is the package manager for node (N-Node P-Package M-Manager). You use third party modules the same way you use your own modules.
- ### 6-1 Your First `npm install` Ever
    install the [pretty-bytes](https://www.npmjs.com/package/pretty-bytes) package, and write a program thats prints the size of a file. don't forget to use the package you have just installed.
     ```bash
    node 5/1/index.js mySong.mp3
    # print 'the size is 6.54 MB'
    ```